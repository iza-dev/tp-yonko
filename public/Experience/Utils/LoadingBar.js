import * as THREE from "three";
import { gsap } from "gsap";

export default class LoadingBar {
  constructor() {
    this.loadingBarElement = document.getElementById("loading-bar");
    this.init();
  }

  init() {
    this.loadingBar = new THREE.LoadingManager(
      () => {
        window.setTimeout(() => {
          gsap.to(overlayMaterial.uniforms.uAlpha, {
            duration: 3,
            value: 0,
            delay: 1,
          });

          loadingBarElement.classList.add("ended");
          loadingBarElement.style.transform = "";
        }, 500);
      },

      (_, itemsLoaded, itemsTotal) => {
        const progressRatio = itemsLoaded / itemsTotal;
        loadingBarElement.style.transform = `scaleX(${progressRatio})`;
      }
    );
  }
}
