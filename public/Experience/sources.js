export default [
  {
    name: "torigateModel",
    type: "gltfModel",
    path: "models/torigate-processed.glb",
  },
  {
    name: "dragonModel",
    type: "gltfModel",
    path: "models/dragon.glb",
  },
];
