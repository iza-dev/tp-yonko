import * as THREE from "three";
import Prefab from "../Prefab.js";

export default class Environment {
  constructor() {
    this.prefab = new Prefab();
    this.scene = this.prefab.scene;
    this.resources = this.prefab.resources;

    this.setSunLight();
  }

  setSunLight() {
    this.sunLight = new THREE.DirectionalLight("#ffffff", 4);
    this.sunLight.castShadow = true;
    this.sunLight.shadow.camera.far = 15;
    this.sunLight.shadow.mapSize.set(1024, 1024);
    this.sunLight.shadow.normalBias = 0.05;
    this.sunLight.position.set(3.5, 2, 1.25);
    this.scene.add(this.sunLight);
  }
}
