import * as THREE from "three";
import gsap from "gsap";
import Prefab from "../Prefab.js";

export default class Dragon {
  constructor() {
    this.prefab = new Prefab();
    this.scene = this.prefab.scene;
    this.resources = this.prefab.resources;
    this.time = this.prefab.time;

    this.resource = this.resources.items.dragonModel;
    this.isScrollEnter = false;

    this.mediaQueryMobile = window.matchMedia("(max-width: 400px)");
    this.mediaQueryTabletPC = window.matchMedia("(min-width: 700px)");

    this.setModel();
    this.setAnimation();
  }
  setModel() {
    this.model = this.resource.scene;
    this.model.position.set(0, -15, 50);
    if (this.mediaQueryMobile.matches) this.model.scale.set(0.6, 0.6, 0.6);
    this.scene.add(this.model);

    this.model.traverse((child) => {
      if (child instanceof THREE.Mesh) {
        child.frustumCulled = false;
      }
    });
  }

  setAnimation() {
    this.animation = {};

    this.animation.mixer = new THREE.AnimationMixer(this.model);

    this.animation.actions = {};

    this.animation.actions.fly = this.animation.mixer.clipAction(
      this.resource.animations[0]
    );

    this.animation.actions.current = this.animation.actions.fly;
    this.animation.actions.current.play();
  }

  setAnimationScroll() {
    this.TLScroll = new gsap.timeline({
      scrollTrigger: {
        trigger: ".webgl",
        start: "top",
        end: "bottom +=30%",
        scrub: 1,
        invalidateOnRefresh: true,
        onLeave: () => {
          this.setAnimationRotate();
          this.TLScroll.kill(true);
        },
      },
    }).to(this.model.position, { z: -28, duration: 2, ease: "inOut" });
  }
  setAnimationRotate() {
    this.TLRotate = new gsap.timeline()
      .to(
        this.model.rotation,
        {
          y: Math.PI * -0.5,
          duration: 3,
        },
        0
      )
      .to(
        this.model.position,
        {
          y: this.mediaQueryMobile.matches ? -12 : -20,
          duration: 3,
        },
        0
      )
      .to(
        this.model.position,
        {
          x: -5,
          duration: 3,
        },
        0
      );
  }

  update() {
    this.animation.mixer.update(this.time.delta * 0.001);
    if (this.isScrollEnter) {
      this.isScrollEnter = false;
      this.setAnimationScroll();
    }
  }
}
