import * as THREE from "three";
import Prefab from "../Prefab.js";
import fragment from "../shaders/flag/fragment.glsl";
import vertex from "../shaders/flag/vertex.glsl";

export default class Flags {
  constructor() {
    this.prefab = new Prefab();
    this.scene = this.prefab.scene;

    this.totalFlags = 2;
    this.clock = new THREE.Clock();

    //to detect when the scroll get the end's point
    //And dispaly the flags
    this.isScrollLeave = false;

    this.textureLoader = new THREE.TextureLoader();
    this.flagTexture = this.textureLoader.load("/textures/flag-japan.jpg");

    this.mediaQueryMobile = window.matchMedia("(max-width: 400px)");
    this.mediaQueryTabletPC = window.matchMedia("(min-width: 700px)");

    this.setGeometry();
    this.setMaterial();
  }
  setGeometry() {
    this.geometry = new THREE.PlaneGeometry(3, 2);
  }
  setMaterial() {
    this.material = new THREE.ShaderMaterial({
      vertexShader: vertex,
      fragmentShader: fragment,
      uniforms: {
        uFrequency: { value: new THREE.Vector2(20, 15) },
        uTime: { value: 0 },
        uColor: { value: new THREE.Color("orange") },
        uTexture: { value: this.flagTexture },
      },
    });
  }
  setMeshes() {
    for (let index = 0; index < this.totalFlags; index++) {
      this.mesh = new THREE.Mesh(this.geometry, this.material);
      if (this.mediaQueryMobile.matches) {
        this.mesh.scale.set(0.8, 0.8, 0.8);
        if (index + 1 === this.totalFlags) {
          this.setUpMesh(5, -Math.PI * 0.05);
        } else {
          this.setUpMesh(-5, Math.PI * 0.05);
        }
      } else if (this.mediaQueryTabletPC.matches) {
        if (index + 1 === this.totalFlags) {
          this.setUpMesh(12.5, -Math.PI * 0.05);
        } else {
          this.setUpMesh(-12.5, Math.PI * 0.05);
        }
      } else {
        this.mesh.scale.set(0.8, 0.8, 0.8);
        if (index + 1 === this.totalFlags) {
          this.setUpMesh(7, -Math.PI * 0.05);
        } else {
          this.setUpMesh(-7, Math.PI * 0.05);
        }
      }
      this.scene.add(this.mesh);
    }
  }
  setUpMesh(positionX, rotationZ) {
    this.mesh.rotation.z = rotationZ;
    this.mesh.position.set(positionX, 3, 0);
  }
  update() {
    const elapsedTime = this.clock.getElapsedTime();
    this.material.uniforms.uTime.value = elapsedTime;
    if (this.isScrollLeave) {
      this.isScrollLeave = false;
      this.setMeshes();
    }
  }
}
