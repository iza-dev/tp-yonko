import Prefab from "../Prefab.js";
import Environment from "./Environment.js";
import ToriGate from "./ToriGate.js";
import Flags from "./Flags.js";
import Dragon from "./Dragon.js";
export default class World {
  constructor() {
    this.prefab = new Prefab();
    this.scene = this.prefab.scene;
    this.resources = this.prefab.resources;

    this.resources.on("ready", () => {
      this.torigate = new ToriGate();
      this.flags = new Flags();
      this.dragon = new Dragon();
      this.environment = new Environment();
    });

    this.mediaQueryMobile = window.matchMedia("(max-width: 400px)");
    this.mediaQueryTabletPC = window.matchMedia("(min-width: 700px)");
  }
  update() {
    if (this.flags) this.flags.update();
    if (this.dragon) this.dragon.update();
    if (this.torigate && this.torigate.isScrollLeave) {
      this.torigate.isScrollLeave = false;
      this.flags.isScrollLeave = true;
    }
    if (this.torigate && this.torigate.isScrollEnter) {
      this.torigate.isScrollEnter = false;
      this.dragon.isScrollEnter = true;
    }
  }

  resize() {
    if (this.mediaQueryMobile.matches) {
      this.torigate.model.scale.set(0.5, 0.5, 0.5);
      this.dragon.model.scale.set(0.6, 0.6, 0.6);
      this.dragon.model.position.y = -12;
    } else if (this.mediaQueryTabletPC.matches) {
      this.torigate.model.scale.set(0.8, 0.8, 0.8);
      this.dragon.model.scale.set(1, 1, 1);
      this.dragon.model.position.y = -20;
    }
  }
}
