import * as THREE from "three";
import gsap from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
import Prefab from "../Prefab.js";

export default class ToriGate {
  constructor() {
    this.prefab = new Prefab();
    this.scene = this.prefab.scene;
    this.resources = this.prefab.resources;
    this.time = this.prefab.time;
    this.resource = this.resources.items.torigateModel;

    this.isScrollLeave = false;
    this.isScrollEnter = false;

    this.mediaQueryMobile = window.matchMedia("(max-width: 400px)");
    this.mediaQueryTabletPC = window.matchMedia("(min-width: 700px)");

    this.setModel();
    this.setAnimationFirst();
  }

  setModel() {
    this.model = this.resource.scene;
    if (this.mediaQueryMobile.matches) this.model.scale.set(0.5, 0.5, 0.5);
    if (this.mediaQueryTabletPC.matches) this.model.scale.set(0.8, 0.8, 0.8);

    this.scene.add(this.model);
  }

  setAnimationFirst() {
    this.TLFirst = new gsap.timeline({
      defaults: { ease: "power3.inOut" },
      onComplete: () => {
        window.document.getElementById("container").classList.toggle("active");
        window.document
          .getElementById("scroll-down")
          .classList.toggle("active");
        this.setUpAnimationScroll();
      },
    });
    this.TLFirst.to(
      this.model.rotation,
      {
        duration: 1,
        x: 0.3,
      },
      1
    )
      .to(
        this.model.position,
        {
          duration: 2,
          y: -2,
        },
        1
      )
      .to(
        this.model.rotation,
        {
          duration: 3,
          y: -3.1,
          ease: "elastic.in",
        },
        2
      )
      .to(
        this.model.position,
        {
          duration: 3,
          y: 3,
        },
        3
      )
      .to(
        this.model.position,
        {
          duration: 2,
          y: -1,
        },
        5
      )
      .fromTo(
        this.model.rotation,
        { z: 0 },
        {
          duration: 3,
          z: 0.1,
        },
        1
      )
      .to(this.model.rotation, { z: 0, duration: 1, ease: "inOut" }, 4)
      .fromTo(
        this.model.position,
        {
          y: -1,
        },
        {
          y: 1,
          duration: 1.5,
          ease: "inOut",
        },
        7
      )
      .to(
        this.model.position,
        {
          y: 0,
          duration: 1.5,
        },
        8.5
      )
      .to(
        this.model.rotation,
        {
          duration: 1,
          x: 0,
          ease: "inOut",
        },
        5
      );
  }

  setUpAnimationScroll() {
    gsap.registerPlugin(ScrollTrigger);
    ScrollTrigger.matchMedia({
      "(prefers-reduced-motion:no-preference)":
        this.setAnimationScroll.bind(this),
    });
    ScrollTrigger.config({
      autoRefreshEvents: "visibilitychange,DOMContentLoaded,load",
      ignoreMobileResize: true,
    });
  }

  setAnimationScroll() {
    this.TLScroll = new gsap.timeline({
      scrollTrigger: {
        trigger: ".webgl",
        start: "top",
        end: "bottom +=20%",
        scrub: 1,
        invalidateOnRefresh: true,
        onLeave: () => {
          this.TLScroll.kill(true);
          window.document.getElementById("text").classList.toggle("active");
          window.document
            .getElementById("container")
            .classList.toggle("active");
          this.isScrollLeave = true;
        },
        onEnter: () => {
          window.document
            .getElementById("scroll-down")
            .classList.remove("active");
          this.isScrollEnter = true;
        },
      },
    }).to(this.model.position, { z: 20, duration: 4, ease: "inOut" }, 0);
  }
}
